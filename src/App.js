import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="container">

      <header className="head">
        <p>Logo</p>
      </header>
      <body className="body">
        <aside className="filter">
          <p>Filter</p>
        </aside>
        <section className="products">
          <ol className="product-list">
            <li className="product-items">
              <p>Product</p>
            </li>
            <li className="product-items">
              <p>Product</p>
            </li>
            <li className="product-items">
              <p>Product</p>
            </li>
            <li className="product-items">
              <p>Product</p>
            </li>
            <li className="product-items">
              <p>Product</p>
            </li>
            <li className="product-items">
              <p>Product</p>
            </li>
            <li className="product-items">
              <p>Product</p>
            </li>
            <li className="product-items">
              <p>Product</p>
            </li>
            <li className="product-items">
              <p>Product</p>
            </li>
            <li className="product-items">
              <p>Product</p>
            </li><li className="product-items">
              <p>Product</p>
            </li>
            <li className="product-items">
              <p>Product</p>
            </li>
            <li className="product-items">
              <p>Product</p>
            </li>
            <li className="product-items">
              <p>Product</p>
            </li>
            <li className="product-items">
              <p>Product</p>
            </li>
            <li className="product-items">
              <p>Product</p>
            </li>
            <li className="product-items">
              <p>Product</p>
            </li>
            <li className="product-items">
              <p>Product</p>
            </li>
            <li className="product-items">
              <p>Product</p>
            </li>
            <li className="product-items">
              <p>Product</p>
            </li>
          </ol>
        </section>
      </body>
      <footer className="foot">
        <p className="footer-content">Footer</p>
      </footer>
    </div>
  );
}

export default App;
